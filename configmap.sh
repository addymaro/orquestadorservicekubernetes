#!/bin/bash

APPLICATION_NAME="orquestadorservice"

set -e
TEMP_DIRECTORY=$(mktemp --directory)
trap "rm -rf \"$TEMP_DIRECTORY\"" EXIT
if kubectl get configmap "$APPLICATION_NAME" -o json > "$TEMP_DIRECTORY/current.json" ; then
  set -e
  kubectl create configmap "$APPLICATION_NAME" --from-file=./configmap/ --dry-run -o json --save-config=false > "$TEMP_DIRECTORY/new.json"
  python3 -c '
import json
import sys
import hashlib

c = json.load(open(sys.argv[1],"r"))
n = json.load(open(sys.argv[2],"r"))

null_exceptions = ["creationTimestamp"]

def recursive_compare(a, b):
    if isinstance(a, (list, dict)) and isinstance(b, (list, dict)):
        for k in a:
            if k in null_exceptions:
                print("null exception key %s" % k)
                if a[k] != None:
                    return False
            elif k in b:
                print("recursive comparing key %s" % k)
                if not recursive_compare(a[k], b[k]):
                    return False
            else:
                print("key \"%s\" is not in present in current ConfigMap" % k)
                return False
        return True
    else:
        ha = hashlib.sha1(a.encode("utf-8")).hexdigest()
        hb = hashlib.sha1(b.encode("utf-8")).hexdigest()
        if ha == hb:
            print("Ok: Same hash")
            return True
        else:
            print("Fail: Values differs")
            print(hashlib.sha1(a.encode("utf-8")).hexdigest())
            print(hashlib.sha1(b.encode("utf-8")).hexdigest())
            return False

if recursive_compare(n, c):
    print("Hooray!, not required replacement")
    exit(0)
else:
    print("Discrepancy detected, required replacement!")
    exit(1)
' "$TEMP_DIRECTORY/current.json" "$TEMP_DIRECTORY/new.json" ||
  kubectl replace -f "$TEMP_DIRECTORY/new.json"
  rm -f "$TEMP_DIRECTORY/current.json" "$TEMP_DIRECTORY/new.json"
else
  kubectl create configmap "$APPLICATION_NAME" --from-file=./configmap/
fi
